﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.DB
{
    class DBManager
    {
        private static SqlConnection connection;

        private static string connectionStr = "Data Source=DESKTOP-LB69HAT;Initial Catalog=SAP_Bonita;User ID=sa;Password=123";

        private delegate void Read(SqlDataReader reader);

        public static List<int> GetOrdersNotIn(string ids)
        {
            var idsNotIn = new List<int>();

            LoopOnRecords("SELECT * FROM Order WHERE persistenceId NOT IN (" + ids + ");", (reader) =>
            {
                idsNotIn.Add(reader.GetInt32(0));
            });

            return idsNotIn;
        }


        private static void LoopOnRecords(string sql, Read callback)
        {
            var command = new SqlCommand(sql, GetConnection());
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                callback(reader);
            }

            connection.Close();
        }

        private static SqlConnection GetConnection()
        {
            if (connection == null) connection = new SqlConnection(connectionStr);

            if (connection.State != System.Data.ConnectionState.Open) connection.Open();

            return connection;
        }
    }
}
