﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SAPBusinessOne
{
    [Serializable()]
    [XmlRoot("ServerSapInfo")]
    public class ServerSapInfo
    {
        public string Server { get; set; }

        public string DbUserName { get; set; }

        public string DbPassword { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string CompanyDB { get; set; }

        public string LicenseServer { get; set; }

        public string SLDServer { get; set; }

        public string WharehouseCode { get; set; }
    }
}
