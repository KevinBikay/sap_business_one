﻿using SAPBusinessOne.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SAPBusinessOne.Services;
using SAPBusinessOne.Services.Server;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using SAPBusinessOne.Services.Models.Orders;
using SAPBusinessOne.Services.Models.Projects;
using SAPbobsCOM;
using SAPBusinessOne.Services.Logs;
using SAPBusinessOne.Services.Models;
using System.ComponentModel;

namespace SAPBusinessOne
{
    class SapApplicationContext : ApplicationContext
    {
        private NotifyIcon trayIcon;

        private SAPbobsCOM.Company Company;

        private BonitaApi api = new BonitaApi();

        private TcpServer SapTcpServer;

        public SapApplicationContext()
        {
            // Initialize Tray Icon
            trayIcon = new NotifyIcon()
            {
                Icon = Resources.AppIcon,
                ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Exit", Exit)
                }),
                Visible = true
            };

            Start();
        }

        void Start()
        {
            int port = 8000;
            SapTcpServer = new TcpServer(IPAddress.Any, port);

            if(!SapTcpServer.isPortAvailable(port))
            {
                MessageBox.Show("Port is already in use, check if the app is not already running", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Application.Exit();
            }

            SapTcpServer.Start(OnReceiveDataFromServer, OnServerStarted, OnTcpServerStopped);
            
            var info = Sap.GetInfo();
            
            if (Connect())
            {
                MessageBox.Show("Connection Success");
            }
            else
            {
                MessageBox.Show(string.Format("Connection failed {0}", Company.GetLastErrorDescription()));
                MessageBox.Show("Identifiants: " + "\r\n" +
                       "Server: " + info.Server + "\r\n" +
                       "DbUserName: " + info.DbUserName + "\r\n" +
                       "DbPassword: " + info.DbPassword + "\r\n" +
                       "UserName: " + info.UserName + "\r\n" +
                       "Password: " + info.Password + "\r\n" +
                       "CompanyDB: " + info.CompanyDB + "\r\n");
            }
        }

        public bool Connect()
        {
            Company = new SAPbobsCOM.Company();

            Company.language = BoSuppLangs.ln_English;
            Company.DbServerType = BoDataServerTypes.dst_MSSQL2014;

            var info = Sap.GetInfo();

            Company.Server = info.Server.Trim();
            Company.DbUserName = info.DbUserName.Trim();
            Company.DbPassword = info.DbPassword.Trim();
            Company.UserName = info.UserName.Trim();
            Company.Password = info.Password.Trim();
            Company.CompanyDB = info.CompanyDB.Trim();

            Company.LicenseServer = info.LicenseServer;
            Company.SLDServer = info.SLDServer;

            Company.UseTrusted = false;

            int connectResult = Company.Connect();

            return connectResult == 0;
        }

        public void OnServerStarted(bool isStarted)
        {
            if (isStarted)
            {
                MessageBox.Show("Server Started");
            }
            else
            {
                MessageBox.Show("Port is already in use, check if the app is not already running", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

                System.Environment.Exit(0);
            }
        }

        public void OnTcpServerStopped(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error != null)
            {
                Logger.Log(e.Error);
                
                MessageBox.Show("The TCP Server has stopped unexpectedlly, check if the app is not already running", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

                System.Environment.Exit(0);
            }
        }

        public TransactionStatus OnReceiveDataFromServer(object sender, string data)
        {
            try
            {
                return Sap.SendModel(data, Company);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return new TransactionStatus("Error insert: " + ex.Message, false);
            }
        } 
        
        void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            trayIcon.Visible = false;

            SapTcpServer.Stop();

            Application.Exit();
        }
    }
}
