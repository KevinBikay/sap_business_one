﻿using Newtonsoft.Json.Linq;
using SAPBusinessOne.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SAPBusinessOne.Extensions
{
    public static class JObjectExtensions
    {
        public static bool CanBeParsedTo<T>(this JObject obj) where T : class
        {

            foreach (FieldInfo ppty in  typeof(T).GetFields())
            {
                if (obj[ppty.Name] == null && Attribute.IsDefined(ppty, typeof(OptionalAttribute)) == false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
