﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Logs
{
    public class Logger
    {
        public static void Log(Exception ex)
        {
            WriteLog(ex.Message, ex.StackTrace);
        }

        public static void Log(string message)
        {
            WriteLog(message);
        }

        public static string Dump(int nbLines = 10)
        {
            if (!File.Exists("./" + GetFileName())) return string.Empty;

            var lines = File.ReadAllLines("./" + GetFileName());

            string content = string.Empty;

            var count = 0;

            for(var i= lines.Length-1; i>=0 && count < nbLines; i--)
            {
                content += lines[i] + "\r\n";
                count++;
            }

            return content;
        }

        private static void WriteLog(string message, string info = "")
        {
            try
            {
                StreamWriter sr = File.AppendText("./" + GetFileName());

                sr.WriteLine(DateTime.Now.ToString("dd MM yyyy HH:mm") + " : " + " : " + message);
                if(info != "") sr.WriteLine(info);

                sr.Close();
            }
            catch
            {
                return;
            }
        }

        private static string GetFileName()
        {
            return DateTime.Now.ToString("yyyy-MM-dd") + ".log";
        }
    }
}
