﻿using Newtonsoft.Json.Linq;
using SAPBusinessOne.Extensions;
using SAPBusinessOne.Services.Http;
using SAPBusinessOne.Services.Models.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services
{
    class BonitaApi : WebApi
    {
        private string authToken = "example-dummy-not-be-used-value";

        private string domainName = "localhost";

        public BonitaApi()
        {
            SetBaseUrl(string.Format("http://{0}:39353", domainName));
        }

        public void Connect()
        {
            var res = Post("/bonita/loginservice", new StringContent("username=walter.bates&password=bpm&redirect=false", Encoding.UTF8, "application/x-www-form-urlencoded"));

            foreach (var cookie in res.Message.Cookies)
            {
                AddHeader(new KeyValuePair<string, string>(cookie.Name, cookie.Value));
            }

            authToken = res.Message.Cookies.Where(c => c.Name == "X-Bonita-API-Token").FirstOrDefault().Value;
        }

        public OrderList GetOrderList()
        {
            return Get<OrderList>("/bonita/API/bdm/businessData/ats.model.DistributionOrder?p=0&c=20&q=findByStatus&f=status=pending", null).Content;
        }

        public OrderItemList GetOrderItemList(string persistenceID)
        {
            return Get<OrderItemList>("/bonita/API/bdm/businessData/ats.model.DistributionOrder/" + persistenceID + "/orderItems", null).Content;
        }
    }
}
