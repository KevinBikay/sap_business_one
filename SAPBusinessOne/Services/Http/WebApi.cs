﻿using Newtonsoft.Json.Linq;
using SAPBusinessOne.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static SAPBusinessOne.Services.Http.JsonRequester;

namespace SAPBusinessOne.Services.Http
{
    class WebApi
    {
        private string baseURL = "";

        private string contentType = DEFAULT_API_CONTENT_TYPE;

        private string accept = "application/json";

        private const string DEFAULT_API_CONTENT_TYPE = "x-www-form-urlencoded; charset=utf-8";

        private List<KeyValuePair<string, string>> headers = new List<KeyValuePair<string, string>>();

        public JsonResponse<T> Post<T>(string endUrl, HttpContent content) where T : class
        {
            return SendRequest<T>(endUrl, HttpMethod.Post, content);
        }

        public JsonResponse<T> Get<T>(string endUrl, HttpContent content) where T : class
        {
            return SendRequest<T>(endUrl, HttpMethod.Get, content);
        }

        public JsonResponse<dynamic> Post(string endUrl, HttpContent content)
        {
            return SendRequest(endUrl, HttpMethod.Post, content);
        }

        public JsonResponse<dynamic> Get(string endUrl, HttpContent content)
        {
            return SendRequest(endUrl, HttpMethod.Get, content);
        }

        public JsonResponse<T> SendRequest<T>(string endUrl, HttpMethod method, HttpContent content) where T : class
        {
            return GetApiResult<T>(endUrl, method, content);
        }

        public JsonResponse<dynamic> SendRequest(string endUrl, HttpMethod method, HttpContent content)
        {
            return GetApiResult(endUrl, method, content);
        }

        public WebApi WithContentType(string contentType)
        {
            this.contentType = contentType;

            return this;
        }

        public WebApi Accept(string accept)
        {
            this.accept = accept;

            return this;
        }

        public JsonResponse<T> GetApiResult<T>(string endUrl, HttpMethod method, HttpContent content) where T : class
        {
            var result = JsonRequester.Request<T>(baseURL, endUrl, method, content, headers);

            return result;
        }

        public JsonResponse<dynamic> GetApiResult(string endUrl, HttpMethod method, HttpContent content)
        {
            var result = JsonRequester.Request<dynamic>(baseURL, endUrl, method, content, headers);

            return result;
        }

        public void SetBaseUrl(string url)
        {
            baseURL = url;
        }

        protected void AddHeader(KeyValuePair<string, string> element)
        {
            headers.Add(element);
        }

        protected string GetFullUrl(string endUrl)
        {
            return this.baseURL + endUrl;
        }
    }
}
