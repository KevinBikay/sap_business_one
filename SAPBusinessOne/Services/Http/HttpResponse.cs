﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace SAPBusinessOne.Services.Http
{
    public class HttpResponse
    {
        public List<Cookie> Cookies { get; set; }

        private HttpResponseMessage ResponseMessage { get; set; }

        public HttpResponse(HttpResponseMessage response)
        {
            ResponseMessage = response;
        }

        public HttpContent Content
        {
            get
            {
                return ResponseMessage.Content;
            }
        }
    }
}
