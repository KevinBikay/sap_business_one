﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SAPBusinessOne.Extensions;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using static SAPBusinessOne.Services.Http.WebRequester;

namespace SAPBusinessOne.Services.Http
{
    public static class JsonRequester
    {
        public class JsonResponse<T> where T : class
        {
            public T Content { get; private set; }

            public HttpResponse Message { get; private set; }

            public JsonResponse(HttpResponse response)
            {
                var content = GetResponseBody(response).Result;

                try
                {
                    Content = JsonConvert.DeserializeObject<T>(content);
                }
                catch
                {
                    Content = new JObject().ToObject<T>();
                }

                Message = response;
            }

            protected async Task<string> GetResponseBody(HttpResponse response)
            {
                string body = await response.Content.ReadAsStringAsync();

                return body;
            }
        }

        public static JsonResponse<T> Request<T>(string baseUrl, string path, HttpMethod method, HttpContent content, List<KeyValuePair<string, string>> headers) where T : class
        {
            var response = WebRequester.GetResponseAsync(baseUrl, path, method, content, headers);

            return new JsonResponse<T>(response);
        }
    }
}
