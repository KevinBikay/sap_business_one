﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SAPBusinessOne.Services.Http
{
    public static class WebRequester
    {
        public delegate void RequestDone(string response, int code);

        private static CookieContainer cookieContainer = new CookieContainer();

        public static HttpResponse GetResponseAsync(string baseUrl, string path, HttpMethod method, HttpContent content, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClientHandler = new HttpClientHandler
            {
                CookieContainer = cookieContainer
            })

            using (var client = new HttpClient(httpClientHandler))
            {
                client.BaseAddress = new Uri(baseUrl);

                var req = new HttpRequestMessage(method, path);

                if(method != HttpMethod.Get) req.Content = content;

                req.Headers.Add("Accept", "application/json");
                req.Headers.Add("Cache-Control", "no-cache");
                
                foreach(var header in headers)
                {
                    req.Headers.Add(header.Key, header.Value);
                }

                HttpResponse response = new HttpResponse(client.SendAsync(req).Result);

                response.Cookies = cookieContainer.GetCookies(client.BaseAddress).Cast<Cookie>().ToList();

                return response;
            }
        }
    }
}
