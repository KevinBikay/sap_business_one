﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SAPBusinessOne.Services
{
    public class HttpResponse
    {
        public string content;

        public int code;

        public WebHeaderCollection headers;

        public CookieCollection cookies;

        public HttpResponse WithCookies(CookieCollection cookies)
        {
            this.cookies = cookies;

            return this;
        }

        public HttpResponse(string content, int code, WebHeaderCollection headers)
        {
            this.content = content;
            this.code = code;
            this.headers = headers;
        }
    }
}
