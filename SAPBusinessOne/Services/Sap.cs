﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SAPbobsCOM;
using SAPBusinessOne.Services.Logs;
using SAPBusinessOne.Services.Models;
using SAPBusinessOne.Services.Models.Orders;
using SAPBusinessOne.Services.Models.Projects;
using SAPBusinessOne.Services.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace SAPBusinessOne.Services
{
    class Sap
    {
        private static string currency = "$";
        private static string wharehouseCode = "08";

        private class Transaction
        {
            public string type;
        }

        private static ServerSapInfo info;

        public static ServerSapInfo GetInfo()
        {
            if (info != null) return info;

            XmlSerializer ser = new XmlSerializer(typeof(ServerSapInfo));
            string filepath = Directory.GetCurrentDirectory() + @"\ServerSapInfo.xml";
            StreamReader reader = new StreamReader(filepath);
            info = (ServerSapInfo)ser.Deserialize(reader);

            return info;
        }

        public static TransactionStatus SendModel(string data, SAPbobsCOM.Company oCompany)
        {
            var t = JsonConvert.DeserializeObject<Transaction>(data);

            try
            {
                switch (t.type.ToLower())
                {
                    case "order":
                    case "app\\_models\\_order":
                        return AddOrder(ParseData<Order>(data), oCompany);
                    case "project":
                    case "app\\_models\\_project":
                        return AddProject(ParseData<Models.Projects.Project>(data), oCompany);
                    case "query-sap":
                        var p = ParseData<Models.Projects.Query>(data);
                        var price = FindPriceListByName(oCompany, p.PriceList);

                        if(price == null) return new TransactionStatus("BOM PRICE NOT FOUND", false);

                        return new TransactionStatus("BOM PRICE: " + GetBOMPrice(oCompany, p.ItemCode, price.PriceListNo), true);
                    case "update-bom":
                        return UpdateBOMs(ParseData<Models.Projects.Project>(data), oCompany);
                    case "get-logs":
                        return new TransactionStatus(Logger.Dump(), true);
                }
            }
            catch(Exception ex)
            {
                return new TransactionStatus(ex.Message, false);
            }

            Logger.Log("Type not found: " + t.type);

            return new TransactionStatus("Parse error", false);
        }
        
        public static T ParseData<T>(string data) where T : class
        {
            var parsedData = JsonConvert.DeserializeObject<T>(data);
            
            return parsedData;
        }

        public static List<TransactionStatus> AddOrders(List<Order> orders, SAPbobsCOM.Company oCompany)
        {
            var results = new List<TransactionStatus>();

            foreach (var item in orders)
            {
                results.Add(AddOrder(item, oCompany));
            }

            return results;
        }

        public static TransactionStatus AddProject(Models.Projects.Project proj, Company oCompany)
        {
            if (proj.looseItems == null) proj.looseItems = new List<PanelItem>();
            if (proj.panels == null) proj.panels = new List<Panel>();
            
            if(string.IsNullOrEmpty(proj.customerCode)) return new TransactionStatus("You should povide a customer code", false);

            string message = string.Empty;
            string lastMessage = string.Empty;

            //Business Partner
            //var partner = (BusinessPartners)oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners);
            //Logger.Log("Search Partner: " + proj.customerCode);
            //if (!partner.GetByKey(proj.customerCode))
            //{
            //    Logger.Log("No Partner: " + proj.customerCode);
            //
            //    //The Business Partner does not exists, should create it
            //    partner.CardCode = proj.customerCode;
            //    partner.CardName = proj.customerName;
            //
            //    lastMessage = AddDataInSapDB(oCompany, partner);
            //    message += lastMessage;
            //
            //    Logger.Log("Create Partner message: " + lastMessage);
            //}

            //var contact = (Contacts)oCompany.GetBusinessObject(BoObjectTypes.oContacts);
            //int customerCode;
            //if (int.TryParse(proj.customerCode, out customerCode))
            //{
            //    Logger.Log("Search customerCode: " + proj.customerCode);
            //    if (!contact.GetByKey(customerCode))
            //    {
            //        Logger.Log("No customerCode: " + proj.customerCode);
            //        //The Contact does not exists, should create it
            //        contact.ContactPersonCode = customerCode;
            //        lastMessage = AddDataInSapDB(oCompany, contact);
            //        message += lastMessage;
            //
            //        Logger.Log("Create customer message: " + lastMessage);
            //    }
            //}

            if(message != string.Empty) return new TransactionStatus(message, false);

            //Add Opportunity
            //Logger.Log("Add Opportunity");
            //SalesOpportunities opportunity = (SalesOpportunities)oCompany.GetBusinessObject(BoObjectTypes.oSalesOpportunities);
            //opportunity.OpportunityType = OpportunityTypeEnum.boOpSales;
            //opportunity.CardCode = proj.customerCode;
            ////opportunity.ContactPerson = contact.ContactPersonCode;
            //opportunity.StartDate = DateTime.Now;
            //opportunity.Lines.MaxLocalTotal = 10; //Force the total with an amount > 0 (required to create a new opportunity)
            //
            //lastMessage = AddDataInSapDB(oCompany, opportunity);
            //message += lastMessage;
            //Logger.Log("Create opportunity message: " + lastMessage);
            //
            //if (message != string.Empty) return new TransactionStatus(message, false);

            //Create Sales Quotation
            Documents quote = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oQuotations);
            //quote.SalesPersonCode = proj.saleRepName;
            quote.Comments = proj.projectRemarks;
            quote.PayToCode = proj.ptc;

            int panelRow = 0;
            
            Logger.Log("----------------------------------------");
            Logger.Log($"---------START CREATE PROJECT {proj.projectCode}{proj.version}-----------");
            Logger.Log("----------------------------------------");

            var priceListATS = FindPriceListByName(oCompany, "Price List ATS");
            var priceListAP_DDP = FindPriceListByName(oCompany, "Price List ATS");
            var man = FindManufacturerByName(oCompany, "ATS");
            var itemGroup = FindItemGroupByName(oCompany, "PBE Panel");
            var shippingType = FindShippingTypeByName(oCompany, "Land");

            PriceLists = new List<SAPbobsCOM.PriceLists>();

            for (var i=0; i<proj.panels.Count; i++)
            {
                var panel = proj.panels[i];

                Logger.Log("Import Panel: " + (string.IsNullOrEmpty(panel.itemCode) ? "-" : panel.itemCode));

                if (panel.panelItems == null) panel.panelItems = new List<PanelItem>();
                
                //1 - Item Master Data Creation
                SAPbobsCOM.Items itemMasterDataPanel = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
                
                itemMasterDataPanel.ItemCode = GetItemMasterIdAuto(oCompany);
                Logger.Log("GOT ITEM CODE: " + itemMasterDataPanel.ItemCode);

                if(Env.IsProduction())
                {
                    itemMasterDataPanel.UserFields.Fields.Item("U_BU").Value = "P";
                }
                
                var isItemMasterDataExists = false;//itemMasterDataPanel.GetByKey(itemMasterDataPanel.ItemCode);
                
                if(itemGroup != null)
                {
                    //●	Item Group = “PBE Panel” (this should auto-increment Item No.)
                    itemMasterDataPanel.ItemsGroupCode = itemGroup.Number;
                }
                
                //●	Description = projectCode + “ “ +panels[i].panelName
                itemMasterDataPanel.ItemName = proj.projectCode + proj.version + " " + panel.panelName;
                
                itemMasterDataPanel.ItemType = ItemTypeEnum.itItems;
                
                if(man != null)
                {
                    //○	Manufacturer = “ATS”
                    itemMasterDataPanel.Manufacturer = man.Code;
                }
                
                if(shippingType != null)
                {
                    //○	Shipping Type = “Land”
                    itemMasterDataPanel.ShipType = shippingType.Code;
                }
                
                //○	Remarks = “from Bonita Order “ +boqId
                itemMasterDataPanel.ValidRemarks = "from Bonita Order " + proj.persistenceId.ToString();

                //●	View->User Defined Fields-> “All categories” X
                //sboTable.UserFields.Fields.Item("NAME").Value = matrixName.Text;

                //○	Inventory Data > UoM Name = "SET"
                itemMasterDataPanel.InventoryUOM = "SET";

                //○	Sales Data > UoM Name = "SET"
                itemMasterDataPanel.SalesUnit = "SET";

                //UoM group-> Manual (Ref [OUGP].UgpEntry)
                itemMasterDataPanel.UoMGroupEntry = -1;

                //○	Default Warehouse = "08"
                itemMasterDataPanel.DefaultWarehouse = wharehouseCode;
                
                if(!string.IsNullOrEmpty(panel.seriesNo))
                {
                    //Update series description field
                    itemMasterDataPanel.UserFields.Fields.Item("U_SeriesNo").Value = panel.seriesNo;
                }

                double panelUnitPrice = 0;
                foreach (var item in panel.panelItems)
                {
                    if (string.IsNullOrEmpty(item.itemRef)) continue;
                    double itemUnitPrice = item.itemUnitPrice;

                    panelUnitPrice += itemUnitPrice * (item.qtyOrder <= 0 ? 1 : item.qtyOrder);
                }

               // Logger.Log("Update Panel #" + itemMasterDataPanel.ItemCode  + " Price List with unit price " + panelUnitPrice);

                itemMasterDataPanel = AddOrUpdateMasterDataPriceList(itemMasterDataPanel, panelUnitPrice, "Price List ATS");

                if (isItemMasterDataExists)
                {
                    lastMessage = GetDBErrorMessage(oCompany, itemMasterDataPanel.Update());
                }
                else
                {
                    //●	Inventory Item = TRUE
                    itemMasterDataPanel.InventoryItem = BoYesNoEnum.tYES;

                    //●	Sales Item = TRUE
                    itemMasterDataPanel.SalesItem = BoYesNoEnum.tYES;

                    //●	Purshase Item = FALSE
                    itemMasterDataPanel.PurchaseItem = BoYesNoEnum.tNO;

                    //○	Planning Method = “None”
                    itemMasterDataPanel.PlanningSystem = BoPlanningSystem.bop_None;

                    //○	Procurement Method = “Make”
                    itemMasterDataPanel.ProcurementMethod = BoProcurementMethod.bom_Make;

                    //○	Component Warehouse = “From Bill of Materials Line”
                    itemMasterDataPanel.ComponentWarehouse = BoMRPComponentWarehouse.bomcw_BOM;

                    //○	Minimum Order Qty = 0.00
                    itemMasterDataPanel.MinOrderQuantity = 0.00;

                    //○	Issue Method = “Manual”
                    itemMasterDataPanel.IssueMethod = BoIssueMethod.im_Manual;
                    
                    //○	Planning method = "MRP"
                    itemMasterDataPanel.PlanningSystem = BoPlanningSystem.bop_MRP;

                    if (Env.IsProduction())
                    {
                        itemMasterDataPanel.DistributionRules.DistributionRule = proj.bu; //OcrCode - bu
                        itemMasterDataPanel.DistributionRules.DistributionRule2 = proj.bucc; //OcrCode2 - bucc
                        itemMasterDataPanel.DistributionRules.DistributionRule3 = proj.division; //OcrCode3 - division
                    }
                    
                    lastMessage = AddDataInSapDB(oCompany, itemMasterDataPanel);
                    message += lastMessage;
                    Logger.Log("Create item master data message: " + lastMessage);
                }

                //Add Bill Of Materials
                Logger.Log($"Add Bill Of Materials {i+1}/{proj.panels.Count}");
                ProductTrees billOfMaterials = null;

                try
                {
                    billOfMaterials = MakeBOM(proj, panel, oCompany, itemMasterDataPanel, priceListATS, priceListAP_DDP, ref message);
                }
                catch(Exception ex)
                {
                    Logger.Log("Add Bill Of Materials Error: " + ex.Message);

                    return new TransactionStatus(ex.Message, false);
                }
                
                if (panelRow > 0) quote.Lines.Add();

                quote.Lines.SalesPersonCode = proj.saleRepId;
                quote.Lines.SetCurrentLine(panelRow);
                quote.Lines.ItemCode = itemMasterDataPanel.ItemCode;
                quote.Lines.Quantity = panel.panelQty;
                quote.Lines.SerialNum = panel.seriesNo.Length > 17 ? panel.seriesNo.Substring(0, 16) : panel.seriesNo;
                quote.Lines.ItemDescription = panel.panelName;
                quote.Lines.WarehouseCode = wharehouseCode;
                quote.Lines.ProjectCode = proj.projectCode + proj.version;

                double bomPrice = GetBOMPrice(oCompany, billOfMaterials);

                if (bomPrice != 0)
                {
                    quote.Lines.UnitPrice = bomPrice;
                    //Logger.Log("Affect the BOM price to the quotation: " + bomPrice.ToString());
                }

                //status should be open

                if (Env.IsProduction())
                {
                    quote.Lines.COGSCostingCode = "IM";
                    quote.Lines.COGSCostingCode2 = "IME";
                    quote.Lines.COGSCostingCode3 = "SAL";
                }
                
                panelRow++;

               // Logger.Log("Row: " + panelRow + "/" + proj.panels.Count);
            }

            if (proj.looseItems == null) proj.looseItems = new List<PanelItem>();

            Logger.Log("Register loose items");

            //Add Loose items on the quotation
            foreach (var item in proj.looseItems)
            {
                if (string.IsNullOrEmpty(item.itemRef) || !IsItemExists(oCompany, item)) continue;
                
                quote.Lines.Add();
                quote.Lines.SetCurrentLine(panelRow);
                quote.Lines.ItemCode = item.itemRef;
                quote.Lines.Quantity = item.qtyOrder;
                 
                if (Env.IsProduction())
                {
                    quote.Lines.COGSCostingCode = "IM";
                    quote.Lines.COGSCostingCode2 = "IME";
                    quote.Lines.COGSCostingCode3 = "SAL";
                }

                //affect the price of the lost item onto the quotation
                quote.Lines.Price = item.estDdpCost == null ? item.itemUnitPrice : (double)item.estDdpCost;
                quote.Lines.Currency = string.IsNullOrEmpty(currency) ? "" : currency;
                quote.Lines.SalesPersonCode = proj.saleRepId;
                quote.Lines.WarehouseCode = wharehouseCode;
                //message += CreateItem(oCompany, item);
                
                panelRow++;
            }

            //Register the quotation
            Logger.Log("Register quotation");

            quote.CardCode = string.IsNullOrEmpty(proj.customerCode) ? "" : proj.customerCode; //●	Select Customer Code = customerCode(name will auto appear)
            //quote.ContactPersonCode = contact.ContactPersonCode;
            quote.DocDate = DateTime.Now;
            quote.DocDueDate = DateTime.Now.AddDays(10);
            quote.SalesPersonCode = proj.saleRepId;

           // Logger.Log("AddDataInSapDB");

            lastMessage = AddDataInSapDB(oCompany, quote);

            if (string.IsNullOrEmpty(lastMessage)) lastMessage = string.Empty;

           // Logger.Log("added");

            int? quoteNum = null;
            int newKeyInt;

            string newKey = oCompany.GetNewObjectKey();

            if(!string.IsNullOrEmpty(newKey) && int.TryParse(newKey, out newKeyInt))
            {
                if (quote.GetByKey(newKeyInt))
                {
                    quoteNum = quote.DocNum;
                }
            }
            

           // Logger.Log("got new key");

            message += lastMessage;

            Logger.Log("quotation #" + (quoteNum == null ? "" : quote.ToString()) + " created: " + lastMessage);

            if (lastMessage.Trim() == string.Empty && quoteNum != null)
            {
                //quotation have been created, should add this quotation to the Opportunity
                SalesOpportunities opportunity = (SalesOpportunities)oCompany.GetBusinessObject(BoObjectTypes.oSalesOpportunities);

                if(opportunity.GetByKey(proj.sapOppNo))
                {
                    //opportunity exists
                    int lineNum = opportunity.Lines.Count;
                    
                    if (lineNum > 0) opportunity.Lines.Add();
                    
                    opportunity.Lines.SetCurrentLine(lineNum);
                    opportunity.Lines.StartDate = quote.DocDate;
                    opportunity.Lines.PercentageRate = 100;
                    opportunity.Lines.MaxLocalTotal = 15;
                    opportunity.Lines.SalesPerson = proj.saleRepId;
                    opportunity.Lines.StageKey = (int)quoteNum;
                    
                    opportunity.Lines.DocumentType = BoAPARDocumentTypes.bodt_Quotation;
                    opportunity.Lines.DocumentNumber = (int)quoteNum;
                    lastMessage = GetDBErrorMessage(oCompany, opportunity.Update());
                    message += lastMessage;
                }

            }
            
            Logger.Log("Ending transactions with message: " + message);
            Logger.Log("has error? " + (message.Trim() == string.Empty ? "NO" : "YES"));

            if (message.Trim() == string.Empty) return new TransactionStatus(string.Format("Project {0} have been inserted", proj.projectCode + proj.version), true);

            return new TransactionStatus(message, false);
        }

        private static TransactionStatus UpdateBOMs(Models.Projects.Project proj, Company oCompany)
        {
            var msg = string.Empty;

            var man = FindManufacturerByName(oCompany, "ATS");
            var priceApDDP = FindPriceListByName(oCompany, "AP DDP");

            foreach (var panel in proj.panels)
            {

                var billOfMaterials = (ProductTrees)oCompany.GetBusinessObject(BoObjectTypes.oProductTrees);

                if (!billOfMaterials.GetByKey(panel.itemCode))
                {
                    msg += Environment.NewLine + "BOM NOT FOUND WITH ID: " + panel.itemCode;

                    continue;
                }

                foreach (var item in panel.panelItems)
                {
                    var itemIndex = -1;

                    for (var row = 0; row < billOfMaterials.Items.Count; row++)
                    {
                        billOfMaterials.Items.SetCurrentLine(row);

                        if (billOfMaterials.Items.ItemCode == item.itemRef) itemIndex = row;
                    }

                    if (itemIndex == -1)
                    {
                        itemIndex = billOfMaterials.Items.Count;

                        if (billOfMaterials.Items.Count > 0) billOfMaterials.Items.Add();
                    }

                    billOfMaterials.Items.SetCurrentLine(itemIndex);

                    Items itemMasterDataPanelItem = (Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
                    var isItemExists = itemMasterDataPanelItem.GetByKey(item.itemRef);
                    
                    if(!isItemExists)
                    {
                        //the panel item should already exists at the ITM level
                        return new TransactionStatus(msg + Environment.NewLine + "Item master data does not exists for this item #" + item.itemRef, false);
                    }

                    double itemUnitPrice = 0;
                    int priceListNo = 0;
                    PriceLists price = null;
                    string priceListName = string.Empty;

                    if (item.estDdpCost != null && item.estDdpCost > 0)
                    {
                        //if there is an est ddp cost, the price list should be AP DDP
                        price = priceApDDP;
                        priceListName = "AP DDP";
                        itemUnitPrice = (double)item.estDdpCost;
                    }
                    else
                    {
                        //the price should be forced on the BOM with the item unit price
                        priceListName = item.itemPriceList;
                        price = FindPriceListByName(oCompany, priceListName);
                        itemUnitPrice = item.itemUnitPrice;
                    }
                    
                    //retrieve the price from the pricelist name
                    if (price != null)
                    {
                        //if the price list have been retrieved, affect the field Price List on the panel item
                        priceListNo = price.PriceListNo;
                    }

                    //if (priceListName != string.Empty) itemMasterDataPanelItem = AddOrUpdateMasterDataPriceList(itemMasterDataPanelItem, itemUnitPrice, priceListName);
                    
                    billOfMaterials.Items.ItemCode = item.itemRef;
                    billOfMaterials.Items.Quantity = item.qtyOrder;

                    if (priceListNo != 0) billOfMaterials.Items.PriceList = priceListNo;

                    if (!string.IsNullOrEmpty(item.wharehouseCode))
                    {
                        billOfMaterials.Items.Warehouse = item.wharehouseCode;
                    }
                    else
                    {
                        billOfMaterials.Items.Warehouse = wharehouseCode;
                    }
                    
                    if (priceListName == "Price M" || priceListName == "AP DDP")
                    {
                        //si price list est Price M et AP DDP
                        //forcer unit price
                        itemMasterDataPanelItem.AvgStdPrice = itemUnitPrice;
                    }

                    billOfMaterials.Items.UserFields.Fields.Item("U_W_MarkUp").Value = item.margin;
                    billOfMaterials.Items.UserFields.Fields.Item("U_AvgMarkup").Value = item.margin;

                    billOfMaterials.Items.Price = itemUnitPrice;
                    
                    billOfMaterials.Items.Comment = string.IsNullOrEmpty(item.designerRemarks) ? "" : item.designerRemarks;
                    billOfMaterials.Items.ItemType = ProductionItemType.pit_Item;
                    billOfMaterials.Items.Project = (string.IsNullOrEmpty(proj.projectCode) ? "?" : proj.projectCode) + (string.IsNullOrEmpty(proj.version) ? "?" : proj.version);
                    billOfMaterials.Items.DistributionRule = string.IsNullOrEmpty(proj.bu) ? "" : proj.bu; //OcrCode - bu
                    billOfMaterials.Items.DistributionRule2 = string.IsNullOrEmpty(proj.bucc) ? "" : proj.bucc; //OcrCode2 - bucc
                    billOfMaterials.Items.DistributionRule3 = string.IsNullOrEmpty(proj.division) ? "" : proj.division; //OcrCode3 - division
                    billOfMaterials.Items.Currency = string.IsNullOrEmpty(currency) ? "" : currency;

                    if (Env.IsProduction())
                    {
                        //ITT1 U_ItemCost U_EstCost - item cost (e 13.16 est ddp cost but still take the avg price)
                        billOfMaterials.Items.UserFields.Fields.Item("U_EstCost").Value = 0;
                        if (item.estDdpCost != null && item.estDdpCost > 0)
                        {
                            billOfMaterials.Items.UserFields.Fields.Item("U_EstCost").Value = item.estDdpCost == null ? 0 : (double)item.estDdpCost;
                        }

                        if (item.totalCost != null && item.totalCost > 0)
                        {
                            billOfMaterials.Items.UserFields.Fields.Item("U_ItemCost").Value = item.totalCost == null ? 0 : (double)item.totalCost;
                        }

                        if (item.margin > 0)
                        {
                            billOfMaterials.Items.UserFields.Fields.Item("U_W_margin").Value = item.margin;
                        }
                    }
                }

                msg += "#" + panel.itemCode + ":" + billOfMaterials.Update() + "\r\n";
                msg += "Item #" + billOfMaterials.Items.ItemCode + " price updated to: " + billOfMaterials.Items.Price + "\r\n";
            }

            return new TransactionStatus(msg, true);
        }

        //private static ProductTrees UpdateBOM(ProductTrees billOfMaterials, Models.Projects.Project proj, Panel panel, Company oCompany, Items itemMasterDataPanel)
        //{
        //    //refresh item prices
        //
        //    var isBomExists = billOfMaterials.GetByKey(itemMasterDataPanel.ItemCode);
        //    if (!isBomExists) return billOfMaterials;
        //
        //    //int row = 0;
        //    string log = string.Empty;
        //
        //    for(var row=0; row<billOfMaterials.Items.Count; row++)
        //    {
        //        billOfMaterials.Items.SetCurrentLine(row);
        //
        //        var item = panel.panelItems.Find(i => i.itemRef == billOfMaterials.Items.ItemCode);
        //
        //        if (item == null) continue;
        //
        //        if (item.estDdpCost != null && item.estDdpCost > 0)
        //        {
        //            //if there is an est ddp cost, the price list should be AP DDP
        //            var price2 = FindPriceListByName(oCompany, "AP DDP");
        //            if (price2 != null)
        //            {
        //                billOfMaterials.Items.PriceList = price2.PriceListNo;
        //                billOfMaterials.Items.Price = (double)item.estDdpCost;
        //            }
        //        }
        //        else
        //        {
        //            //the price should be forced on the BOM with the item unit price
        //            billOfMaterials.Items.Price = item.itemUnitPrice;
        //        }
        //
        //        log += "Refresh BOM #" + itemMasterDataPanel.ItemCode + " Item #" + item.itemRef + " with price: " + billOfMaterials.Items.Price + "\r\n";
        //    }
        //
        //    Logger.Log(log);
        //
        //    billOfMaterials.Update();
        //
        //    return billOfMaterials;
        //}

        private static List<PriceLists> PriceLists { get; set; } = new List<SAPbobsCOM.PriceLists>();

        private static ProductTrees MakeBOM(Models.Projects.Project proj, Panel panel, Company oCompany, Items itemMasterDataPanel, PriceLists priceListATS, PriceLists priceListAP_DDP, ref string message)
        {
            var billOfMaterials = (ProductTrees)oCompany.GetBusinessObject(BoObjectTypes.oProductTrees);
            
            //Relate the bill of material to the item master data
            billOfMaterials.TreeCode = itemMasterDataPanel.ItemCode;
            billOfMaterials.Warehouse = wharehouseCode;

            //var isBomExists = billOfMaterials.GetByKey(itemMasterDataPanel.ItemCode);
            //billOfMaterials.Quantity = panel.panelQty;
            billOfMaterials.Quantity = 1;
            billOfMaterials.DistributionRule = proj.bu; //OcrCode - bu
            billOfMaterials.DistributionRule2 = proj.bucc; //OcrCode2 - bucc
            billOfMaterials.DistributionRule3 = proj.division; //OcrCode3 - division
            billOfMaterials.Project = proj.projectCode + proj.version;

            //Logger.Log("UPDATE U_W_MarkUp field");
            //billOfMaterials.UserFields.Fields.Item("U_W_MarkUp").Value = "40";
            //billOfMaterials.UserFields.Fields.Item("U_AvgMarkup").Value = 40.0;

            if (priceListATS != null)
            {
                //Logger.Log("SET BOM PriceList with price list Price List ATS no: " + priceListATS.PriceListNo);
                billOfMaterials.PriceList = priceListATS.PriceListNo;
            }

            int row = 0;
            string lastMessage = string.Empty;

            foreach (var item in panel.panelItems)
            {
                //Logger.Log("Check item : " + (string.IsNullOrEmpty(item.itemRef) ? string.Empty : item.itemRef));

                if (string.IsNullOrEmpty(item.itemRef)) continue;

                //Add items to the BOM
                if (row > 0) billOfMaterials.Items.Add();

                //create related item master data for the PBE Panel item
                Items itemMasterDataPanelItem = (Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
                var isItemExists = itemMasterDataPanelItem.GetByKey(item.itemRef);
                
                if(!isItemExists)
                {
                    //the panel item should already exists at the ITM level
                    throw new Exception(lastMessage + Environment.NewLine + "Item master data does not exists for this item #" + item.itemRef);
                }

                double itemUnitPrice = 0;
                int priceListNo = 0;
                PriceLists curPriceList = null;

                if (item.estDdpCost != null && item.estDdpCost > 0)
                {
                    //if there is an est ddp cost, the price list should be AP DDP
                    curPriceList = priceListAP_DDP;
                    itemUnitPrice = (double)item.estDdpCost;
                    
                }
                else
                {
                    if(string.IsNullOrEmpty(item.itemPriceList))
                    {
                        continue;
                    }

                    curPriceList = PriceLists.Find(p => p.PriceListName == item.itemPriceList);

                    if(curPriceList == null)
                    {
                        //the price should be forced on the BOM with the item unit price + the margin
                        curPriceList = FindPriceListByName(oCompany, item.itemPriceList);

                        if (curPriceList != null) PriceLists.Add(curPriceList);
                    }

                    itemUnitPrice = item.itemUnitPrice + item.margin;
                }
                
                //billOfMaterials.Items.UserFields.Fields.Item("U_W_MarkUp").Value = item.margin;
                //billOfMaterials.Items.UserFields.Fields.Item("U_AvgMarkup").Value = item.margin;
                
                //retrieve the price from the pricelist name
                if (curPriceList != null)
                {
                    //if the price list have been retrieved, affect the field Price List on the panel item
                    priceListNo = curPriceList.PriceListNo;
                }
                
                billOfMaterials.Items.SetCurrentLine(row);
                billOfMaterials.Items.Quantity = item.qtyOrder;
                billOfMaterials.Items.ItemCode = item.itemRef;

                if (priceListNo != 0) billOfMaterials.Items.PriceList = priceListNo;

                billOfMaterials.Items.Price = itemUnitPrice;

                if (!string.IsNullOrEmpty(item.wharehouseCode))
                {
                    billOfMaterials.Items.Warehouse = item.wharehouseCode;
                }
                else
                {
                    billOfMaterials.Items.Warehouse = wharehouseCode;
                }

                if(!string.IsNullOrEmpty(itemMasterDataPanelItem.DefaultWarehouse))
                {
                    //the item's wharehouse on the BOM should be equal to the ITM default wharehouse
                    billOfMaterials.Items.Warehouse = itemMasterDataPanelItem.DefaultWarehouse;
                }

                billOfMaterials.Items.Comment = item.designerRemarks;
                billOfMaterials.Items.ItemType = ProductionItemType.pit_Item;
                billOfMaterials.Items.Project = proj.projectCode + proj.version;
                billOfMaterials.Items.DistributionRule = proj.bu; //OcrCode - bu
                billOfMaterials.Items.DistributionRule2 = proj.bucc; //OcrCode2 - bucc
                billOfMaterials.Items.DistributionRule3 = proj.division; //OcrCode3 - division
                billOfMaterials.Items.Currency = currency;
                
                if (Env.IsProduction())
                {
                    //ITT1 U_ItemCost U_EstCost - item cost (e 13.16 est ddp cost but still take the avg price)
                    if (item.estDdpCost != null && item.estDdpCost > 0)
                    {
                        //Logger.Log("Add User field U_EstCost to the BOM : " + item.estDdpCost);
                        billOfMaterials.Items.UserFields.Fields.Item("U_EstCost").Value = item.estDdpCost == null ? 0 : (double)item.estDdpCost;
                    }

                    if (item.totalCost != null && item.totalCost > 0)
                    {
                       // Logger.Log("Add User field U_ItemCost to the BOM : " + item.totalCost);
                        //billOfMaterials.Items.UserFields.Fields.Item("U_ItemCost").Value = item.totalCost == null ? 0 : (double)item.totalCost;
                        billOfMaterials.Items.UserFields.Fields.Item("U_ItemCost").Value = itemUnitPrice;
                    }


                    if (item.margin > 0)
                    {
                       // Logger.Log("Add User field U_W_margin to the BOM : " + item.margin);
                        billOfMaterials.Items.UserFields.Fields.Item("U_W_margin").Value = item.margin;
                    }
                }
                
                row++;
            }

            //if (isBomExists)
            //{
            //    //already exists, we update the BOM
            //    lastMessage = GetDBErrorMessage(oCompany, billOfMaterials.Update());
            //    message += lastMessage;
            //    //Logger.Log("Update BOM #" + itemMasterDataPanel.ItemCode + " message: " + lastMessage);
            //}
            //else
            //{
            lastMessage = AddDataInSapDB(oCompany, billOfMaterials);
            message += lastMessage;
            Logger.Log("Create BOM #" + itemMasterDataPanel.ItemCode + " message: " + lastMessage);
            //}

            return billOfMaterials;
        }

        private static SAPbobsCOM.Items AddOrUpdateMasterDataPriceList(SAPbobsCOM.Items itemMasterDataPanel, double price, string priceListName = null)
        {
            var log = string.Empty;

            //Update the existing prices
            for (var i = 0; i < itemMasterDataPanel.PriceList.Count; i++)
            {
                itemMasterDataPanel.PriceList.SetCurrentLine(i);

                if(string.IsNullOrEmpty(priceListName))
                {
                    //update all of the prices whatever the price list name
                    itemMasterDataPanel.PriceList.Price = price;
                }
                else if(priceListName == itemMasterDataPanel.PriceList.PriceListName)
                {
                    //a price list name have been specified, try to update only the price with the same name
                    itemMasterDataPanel.PriceList.Price = price;
                }

                log += "Master Data Price List #" + itemMasterDataPanel.PriceList.PriceListName + " Updated to: " + itemMasterDataPanel.PriceList.Price + "\r\n";
            }

            if (itemMasterDataPanel.PriceList.Count == 0 && priceListName != null)
            {
                log += "Add item master data price : " + price;

                //no price exists for this item, we should create one
                itemMasterDataPanel.PriceList.SetCurrentLine(0);
                itemMasterDataPanel.PriceList.Price = price;
            }

           // Logger.Log(log);

            return itemMasterDataPanel;
        }
        
        public static TransactionStatus AddOrder(Order order, SAPbobsCOM.Company oCompany)
        {
            if (oCompany == null) return new TransactionStatus("Company not found", false);

            SAPbobsCOM.Documents sboSO = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
            //SAPbobsCOM.Recordset oRecSet;

            sboSO.CardCode = order.customerId;

            sboSO.CardName = order.customerName;

            sboSO.SalesPersonCode = order.saleRepId;

            //Update value of a user-defined field (Customer PO No.) in Sales Order module
            sboSO.UserFields.Fields.Item("U_W_Cus_PONo").Value = order.persistenceId;

            sboSO.Comments = "Comments on order:\n" + order.commentOrder + "\nComments on approval:\n" + order.rejectReason;

            try
            {
                sboSO.DocDate = DateTime.Parse(order.orderDate, CultureInfo.CreateSpecificCulture("en-US"));

                sboSO.DocDueDate = DateTime.Parse(order.requestedDeliveryDate, CultureInfo.CreateSpecificCulture("en-US"));
            }
            catch (Exception e)
            {
                return new TransactionStatus(e.Message + "# " + order.orderDate + " # " + order.requestedDeliveryDate + " --> " + order.persistenceId, false);
            }

            int row = 0;
            string itemsInfo = string.Empty;
            SAPbobsCOM.Items oMM = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);


            foreach (var line in order.Items)
            {
                //ici j'ajoute une ligne dans la commande sinon c'est la 1ere ligne
                //qui est prise par defaut, voila pourquoi if (row > 0), a partir du second tour
                //dans la boucle on ajoute l'element sur la commande, par defaut
                //une commande a toujours une ligne d'items
                if (row > 0) sboSO.Lines.Add();

                //ici on recupere le produit en fonction de sa référence
                var item = oMM.GetByKey(line.itemRef);

                //la on selectionne la bonne ligne de la commande pour pointer sur le bon produit
                sboSO.Lines.SetCurrentLine(row);

                //ici on affecte le numero de reference du produit
                sboSO.Lines.ItemCode = line.itemRef;

                //ici la quantité
                sboSO.Lines.Quantity = line.qtyOrder;

                //la on recupere le prix du produit meme si cette manoeuvre doit
                //etre faite automatiquement
                //var price = GetItemPrice(oCompany, line.itemRef);

                //sboSO.Lines.Price = price * line.qtyOrder;
                sboSO.Lines.UnitPrice = line.unitPrice;

                //ici on recupère le discount (ne fonctionne pas pour l'instant du à un blocage SAP)
                sboSO.Lines.DiscountPercent = line.discountPercent;

                //ici nous ajountons le Sale Rep sur chaque ligne. Attention, il doit etre le meme que le Sale Rep sur la commande. Restriction SAP
                sboSO.Lines.SalesPersonCode = order.saleRepId;

                //ici nous ajoutons manuellement les infos Costing
                sboSO.Lines.COGSCostingCode = "TD";
                sboSO.Lines.COGSCostingCode2 = "TDD";
                sboSO.Lines.COGSCostingCode3 = "DIR";
                sboSO.Lines.COGSAccountCode = "603700";

                itemsInfo += "Code: " + line.itemRef + " - -Qty: " + line.qtyOrder + Environment.NewLine;

                //on passe ensuite sur la commande suivante
                row++;
            }

            string message = string.Empty;
            int lErrCode = 0;
            string sErrMsg = "";
            int lRetCode = sboSO.Add();

            if (lRetCode != 0)
            {
                oCompany.GetLastError(out lErrCode, out sErrMsg);

                if (lErrCode != -4006)
                {
                    message = lErrCode + " " + (sErrMsg == null ? string.Empty : sErrMsg.Replace('"', ' '));
                }
            }
            
            if (message.Trim() == string.Empty)
            {
                if (order.commercialInvoice) return UpdateOrderWithDiscountPrice(sboSO, order, oCompany);

                return new TransactionStatus(string.Format("Order {0} have been inserted", order.persistenceId), true);
            }


            return new TransactionStatus(message, false);
        }

        public static TransactionStatus UpdateOrderWithDiscountPrice(Documents sboSO, Order order, SAPbobsCOM.Company oCompany)
        {
            //refresh invoice
            Logger.Log("Refresh invoice");
            
            if (sboSO.GetByKey(Convert.ToInt32(oCompany.GetNewObjectKey())))
            {
                for (var i=0; i<sboSO.Lines.Count; i++)
                {
                    sboSO.Lines.SetCurrentLine(i);

                   // Logger.Log("Invoice #" + oCompany.GetNewObjectKey() + " Update Line " + i + " Replace PriceAfterVAT " + sboSO.Lines.PriceAfterVAT + " with Price " + sboSO.Lines.Price);

                    //update Gross Price After Disc. (PriceAfterVAT) with Price after discount (Price)

                    sboSO.Lines.PriceAfterVAT = sboSO.Lines.Price;
                }

                string lastMessage = GetDBErrorMessage(oCompany, sboSO.Update());
               // Logger.Log("Update Invoice #" + oCompany.GetNewObjectKey() + " (panel item) message: " + lastMessage);
            }
            else
            {
                Logger.Log("Invoice #" + oCompany.GetNewObjectKey() + " not found -- " + order.persistenceId);
            }

            return new TransactionStatus(string.Format("Order {0} have been inserted", order.persistenceId), true);
        }

        public static List<string> GetItems(SAPbobsCOM.Company oCompany)
        {
            List<string> ids = new List<string>();

            SAPbobsCOM.Items items;
            Recordset oRecordset = (Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            var obj = (SBObob)oCompany.GetBusinessObject(BoObjectTypes.BoBridge);
            oRecordset = obj.GetItemList();
            items = (Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
            
            while (!oRecordset.EoF)
            {
                ids.Add(oRecordset.Fields.Item(0).Value.ToString());
                oRecordset.MoveNext();
            }

            return ids;
        }

        public static double GetItemPrice(Company oCompany, string code, int priceListNo)
        {
            SAPbobsCOM.Items oMM = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);

            if (!oMM.GetByKey(code)) return 0;
            
            for (int p = 0; p < oMM.PriceList.Count; p++)
            {
                oMM.PriceList.SetCurrentLine(p);

                if (oMM.PriceList.PriceList == priceListNo) return oMM.PriceList.Price;
            }

            return 0;
        }

        public static PriceLists FindPriceListByName(Company oCompany, string name)
        {
            //// Get an initialized SBObob object
            var price = (SAPbobsCOM.PriceLists)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPriceLists);
            var oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            
            oRecordSet.DoQuery("SELECT * FROM OPLN WHERE ListName='" + name + "'");
            price.Browser.Recordset = oRecordSet;
            price.Browser.Refresh();

            //// use the RecordSet to set the BusnessPartners object
            //// by using the GetByKey method
            while (!(price.Browser.EoF == true))
            {
                price.Browser.MoveNext();

                return price;
            }

            return null;
        }

        public static double GetBOMPrice(Company oCompany, ProductTrees billOfMaterials)
        {
            return GetBOMPrice(oCompany, billOfMaterials.TreeCode, billOfMaterials.PriceList);
        }

        public static double GetBOMPrice(Company oCompany, string code, int priceListNo)
        {
            return GetItemPrice(oCompany, code, priceListNo);
        }

        public static Manufacturers FindManufacturerByName(Company oCompany, string name)
        {
            //// Get an initialized SBObob object
            var manufacturer = (SAPbobsCOM.Manufacturers)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oManufacturers);
            var oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            oRecordSet.DoQuery("SELECT * FROM OMRC");
            manufacturer.Browser.Recordset = oRecordSet;
            manufacturer.Browser.Refresh();

            //// use the RecordSet to set the BusnessPartners object
            //// by using the GetByKey method
            int i = 0;
            while (!(manufacturer.Browser.EoF == true))
            {
                //// make sure the record set didn't reach the EOF
                if (manufacturer.Browser.EoF == false && i < 100)
                {
                    manufacturer.Browser.MoveNext();
                    
                    if(manufacturer.ManufacturerName.ToLower().Trim() == name.ToLower().Trim())
                    {
                        return manufacturer;
                    }

                    i++;
                }
                else
                {
                    return null;
                }
            }
            
            return null;
        }

        public static ItemGroups FindItemGroupByName(Company oCompany, string name)
        {
            //// Get an initialized SBObob object
            var groups = (SAPbobsCOM.ItemGroups)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItemGroups);
            var oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            oRecordSet.DoQuery("SELECT * FROM OITB");
            groups.Browser.Recordset = oRecordSet;
            groups.Browser.Refresh();

            //// use the RecordSet to set the BusnessPartners object
            //// by using the GetByKey method
            int i = 0;
            while (!(groups.Browser.EoF == true))
            {
                //// make sure the record set didn't reach the EOF
                if (groups.Browser.EoF == false && i < 100)
                {
                    groups.Browser.MoveNext();

                    if (groups.GroupName.ToLower().Trim() == name.ToLower().Trim())
                    {
                        return groups;
                    }

                    i++;
                }
                else
                {
                    return null;
                }
            }
            
            return null;
        }

        public static ShippingTypes FindShippingTypeByName(Company oCompany, string name)
        {
            //// Get an initialized SBObob object
            var shippingTypes = (SAPbobsCOM.ShippingTypes)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oShippingTypes);
            var oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            oRecordSet.DoQuery("SELECT * FROM OSHP");
            shippingTypes.Browser.Recordset = oRecordSet;
            shippingTypes.Browser.Refresh();

            //Logger.Log("Run Shipping Type");
            //// use the RecordSet to set the BusnessPartners object
            //// by using the GetByKey method
            int i = 0;
            while (!(shippingTypes.Browser.EoF == true))
            {
                //// make sure the record set didn't reach the EOF
                if (shippingTypes.Browser.EoF == false && i < 100)
                {
                    shippingTypes.Browser.MoveNext();

                    //Logger.Log("Run Shipping Type " + shippingTypes.Name);

                    if (shippingTypes.Name.ToLower().Trim() == name.ToLower().Trim())
                    {
                        return shippingTypes;
                    }

                    i++;
                }
                else
                {
                    return null;
                }
            }

            return null;
        }

        public static string GetItemMasterIdAuto(Company oCompany)
        {
            string itemCode = GetItemCodePrefixByYear(DateTime.Now.Year);

            var oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            var q = string.Format("SELECT * FROM [OITM] WHERE ItemCode LIKE '{0}%'", itemCode);
            
            oRecordSet.DoQuery(q);

            int count = oRecordSet.RecordCount;
            count++;

            var isItemExists = true;
            SAPbobsCOM.Items oItem = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);

            while (isItemExists)
            {
                var tmpItemCode = itemCode + GetIntToCodeString(count);

                isItemExists = oItem.GetByKey(tmpItemCode);

                if (isItemExists)
                {
                    count++;
                }
            }

            itemCode += GetIntToCodeString(count);

            return itemCode;
        }

        public static string GetItemCodePrefixByYear(int year)
        {
            List<string> letters = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            int startYear = 2020;
            int startIndex = 4;

            int offset = year - startYear;
            int curIndex = startIndex;

            string curLetterSerie = string.Empty;
            for (int i = 0; i < offset; i++)
            {
                if (++curIndex >= letters.Count)
                {
                    curLetterSerie += letters.Last();
                    curIndex = 0;
                }
            }

            curLetterSerie += letters[curIndex];

            if (curLetterSerie.Length == 1) curLetterSerie += "A";

            return "P" + curLetterSerie;
        }

        public static string GetIntToCodeString(int count)
        {
            string code = string.Empty;

            for (var i = count.ToString().Length; i < 5; i++)
            {
                code += '0';
            }

            code += count.ToString();

            return code;
        }
        
        private static string AddDataInSapDB(Company oCompany, dynamic dataToAdd)
        {
            int lRetCode = dataToAdd.Add();

            return GetDBErrorMessage(oCompany, lRetCode);
        }

        private static string GetDBErrorMessage(Company oCompany, int lRetCode)
        {
            string sErrMsg = "";
            
            if (lRetCode != 0)
            {
                oCompany.GetLastError(out lRetCode, out sErrMsg);

                if (lRetCode != -4006)
                {
                    return lRetCode.ToString() + " " + (sErrMsg == null ? string.Empty : sErrMsg.Replace('"', ' ')) + "\r\n";
                }
            }

            return string.Empty;
        }

        private static bool IsItemExists(Company oCompany, PanelItem item)
        {
            SAPbobsCOM.Items oItem = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);

            return oItem.GetByKey(item.itemRef);
        }

        private static string CreateOrUpdateItem(Company oCompany, PanelItem item)
        {
            if (string.IsNullOrEmpty(item.itemRef)) return string.Empty;

            //create oitem
            SAPbobsCOM.Items oItem = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
            
            var isItemExists = oItem.GetByKey(item.itemRef);

            if (isItemExists) return string.Empty;

            oItem.ItemCode = item.itemRef;
            
            oItem.InventoryItem = BoYesNoEnum.tYES;
            //●	Sales Item = TRUE
            oItem.SalesItem = BoYesNoEnum.tYES;
            
            //●	Description = projectCode + “ “ +panels[i].panelName
            oItem.ItemName = item.itemDescription;
            oItem.ForeignName = item.itemDescription;

            //○	Planning Method = “None”
            oItem.PlanningSystem = BoPlanningSystem.bop_None;
            //○	Procurement Method = “Make”
            oItem.ProcurementMethod = BoProcurementMethod.bom_Make;
            //○	Component Warehouse = “From Bill of Materials Line”
            oItem.ComponentWarehouse = BoMRPComponentWarehouse.bomcw_BOM;
            //○	Minimum Order Qty = 0.00
            oItem.MinOrderQuantity = 0.00;
            //○	Issue Method = “Manual”
            oItem.IssueMethod = BoIssueMethod.im_Manual;
            //○	Remarks = “from Bonita Order “ +boqId
            oItem.ValidRemarks = "from Bonita Order " + item.designerRemarks;
            string lastMessage = string.Empty;

            if (isItemExists)
            {
                //already exists, we update the item
                lastMessage = GetDBErrorMessage(oCompany, oItem.Update());
                Logger.Log("Update item #" + oItem.ItemCode + " (panel item) message: " + lastMessage);
            }
            else
            {
                //Does not exists, we create it
                lastMessage = AddDataInSapDB(oCompany, oItem);
                Logger.Log("Create item (panel item) message: " + lastMessage);
            }

            return lastMessage;
        }
    }
}
