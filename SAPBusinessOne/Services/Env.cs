﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services
{
    public static class Env
    {
        public static bool IsDev()
        {
            return Properties.Settings.Default.Env == "dev";
        }

        public static bool IsProduction()
        {
            return Properties.Settings.Default.Env == "production";
        }
    }
}
