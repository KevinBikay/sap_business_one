﻿using Newtonsoft.Json.Linq;
using SAPBusinessOne.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services
{
    class WebApi
    {
        private string baseURL = "";

        private static int nbRequests = 0;

        private string contentType = DEFAULT_API_CONTENT_TYPE;

        private string accept = "application/json";

        private const string DEFAULT_API_CONTENT_TYPE = "x-www-form-urlencoded; charset=utf-8";

        public dynamic Post<T>(string endUrl, dynamic parameters) where T : class
        {
            return WithContentType(WebRequester.WebHeaderInfo.JSON).SendRequest<T>(endUrl, "POST", parameters);
        }

        public dynamic Get<T>(string endUrl, dynamic parameters) where T : class
        {
            return WithContentType(WebRequester.WebHeaderInfo.URL_ENCODED).SendRequest<T>(endUrl, "GET", parameters);
        }

        public dynamic SendRequest<T>(string endUrl, string method, dynamic parameters) where T : class
        {
            nbRequests = 0;

            return GetApiResult<T>(endUrl, method, parameters);
        }

        public WebApi WithContentType(string contentType)
        {
            this.contentType = contentType;

            return this;
        }

        public WebApi Accept(string accept)
        {
            this.accept = accept;

            return this;
        }

        public dynamic GetApiResult<T>(string endUrl, string method, dynamic parameters) where T : class
        {

            nbRequests++;

            JObject result = JsonRequester.Request(GetFullUrl(endUrl), method, parameters, GetHeaderInfo());

            return GetParsedResult<T>(result, endUrl, method, parameters);
        }

        public Task<HttpResponseMessage> GetApiResult(string endUrl, string method, dynamic parameters)
        {
            return WebRequester.GetResponseAsync(GetFullUrl(endUrl), GetHeaderInfo(), method, parameters);
        }

        public void SetBaseUrl(string url)
        {
            baseURL = url;
        }

        protected dynamic GetParsedResult<T>(JObject result, string endUrl, string method, dynamic parameters) where T : class
        {
            if (result.CanBeParsedTo<T>())
            {
                return result.ToObject<T>();
            }

            return null;
        }

        protected System.Net.WebHeaderCollection GetHeaders()
        {

            var headers = new System.Net.WebHeaderCollection();

            headers.Add("Cache-Control", "no-cache");

            if (contentType.Trim() == string.Empty)
            {
                contentType = DEFAULT_API_CONTENT_TYPE;
            }

            if (accept.Trim() == string.Empty)
            {
                accept = DEFAULT_API_CONTENT_TYPE;
            }

            return headers;
        }

        protected virtual WebRequester.WebHeaderInfo GetHeaderInfo()
        {
            return new WebRequester.WebHeaderInfo()
            {
                ContentType = contentType,
                Accept = accept,
                HeaderCollection = GetHeaders()
            };
        }

        protected string GetFullUrl(string endUrl)
        {
            return this.baseURL + endUrl;
        }
    }
}
