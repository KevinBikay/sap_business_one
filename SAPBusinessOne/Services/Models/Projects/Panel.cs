﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Models.Projects
{
    public class Panel
    {
        public string itemCode;
        public string panelName;
        public string itemGroup;
        public string businessUnit;
        public double panelQty;
        public string seriesNo;
        public double margin;
        public double price;
        public double cost;

        public List<PanelItem> panelItems;
    }
}
