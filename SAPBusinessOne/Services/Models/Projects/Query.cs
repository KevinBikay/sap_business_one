﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Models.Projects
{
    public class Query
    {
        public string ItemCode { get; set; }
        public string PriceList { get; set; }
    }
}
