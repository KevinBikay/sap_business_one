﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Models.Projects
{
    public class PanelItem
    {
        public string itemRef;
        public string itemDescription;
        public string itemBrand;
        public string itemGroup;
        public string wharehouseCode;
        public double qtyOrder;
        public double qtyAvailable;
        public double itemCost;
        public double margin;
        public double? itemPrice;
        public double itemUnitPrice;
        public double? estDdpCost;
        public double? totalCost;
        public string designerRemarks;
        public string itemPriceList;
    }
}
