﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Models.Projects
{
    public class Project : IModel
    {
        public int persistenceId;
        public string projectCode;
        public string customerCode;
        public string customerName;
        public string version;
        public string status;
        public string designId;
        public string projectName;
        public string projectRemarks;
        public string ptc;
        public string saleRepName;
        public string bu;
        public string bucc;
        public string division;
        public int saleRepId;
        public int sapOppNo;
        public List<Panel> panels;
        public List<PanelItem> looseItems;
    }
}
