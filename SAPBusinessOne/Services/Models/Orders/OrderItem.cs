﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Models.Orders
{
    class OrderItem
    {
        public string persistenceId = string.Empty;
        public string persistenceVersion_string = string.Empty;
        public string itemRef = string.Empty;
        public string itemDescription = string.Empty;
        public string itemBrand = string.Empty;
        public string itemGroup = string.Empty;
        public int qtyOrder = 0;
        public int qtyOnHand = 0;
        public int discountPercent = 0;
        public double unitPrice = 0.00;

        public string ToCsv(string separator = ";")
        {
            return persistenceId + separator +
                persistenceVersion_string + separator +
                itemRef.Replace(separator, "") + separator +
                itemDescription.Replace(separator, "") + separator +
                itemBrand.Replace(separator, "") + separator +
                itemGroup.Replace(separator, "") + separator +
                qtyOrder + separator +
                qtyOnHand + separator +
                discountPercent;
        }
    }
}
