﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Models.Orders
{
    class Order
    {
        public string persistenceId = string.Empty;
        public string userId = string.Empty;
        public string customerId = string.Empty;
        public string customerName = string.Empty;
        public string customerAlias = string.Empty;
        public string orderDate = string.Empty;
        public bool vatApplicable = false;
        public bool commercialInvoice = false;
        public string requestedDeliveryDate = string.Empty;
        public int saleRepId = 0;
        public string saleRepName = string.Empty;
        public double discountPercent = 0.00;
        public string commentOrder = string.Empty;
        public string rejectReason = string.Empty;

        public OrderItemList Items { get; set; } = new OrderItemList();
    }
}
