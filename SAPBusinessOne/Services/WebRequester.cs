﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SAPBusinessOne.Services
{
    public static class WebRequester
    {
        public delegate void RequestDone(string response, int code);

        public class WebHeaderInfo {

            public const string JSON = "application/json; charset=utf-8";

            public const string URL_ENCODED = "application/x-www-form-urlencoded; charset=utf-8";

            public const string DOWNLOAD = "application/force-download, text/html;";

            public const string ALL = "*/*";

            public string Accept { get; set; } = JSON;

            public string ContentType { get; set; } = URL_ENCODED;

            public bool KeepAlive { get; set; } = true;

            public CookieContainer CookieContainer { get; set; } = new CookieContainer();

            public WebHeaderCollection HeaderCollection { get; set; } = new WebHeaderCollection();
        }

        public static async Task<HttpResponseMessage> GetResponseAsync(string url, WebHeaderInfo headerInfo, string method, dynamic parameters)
        {
            var uri = new Uri(url);

            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Post, uri);

            var requestContent = parameters;
            request.Content = new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded");

            var response = await client.SendAsync(request);

            return response;

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //
            //if (headerInfo.HeaderCollection is WebHeaderCollection) {
            //    request.Headers = headerInfo.HeaderCollection;
            //}
            //
            //request.Accept = headerInfo.Accept;
            //request.ContentType = headerInfo.ContentType;
            //request.Method = method;
            //request.KeepAlive = headerInfo.KeepAlive;
            //request.CookieContainer = headerInfo.CookieContainer;
            //
            //if (request.Method != "GET")
            //{
            //    string content = string.Empty;
            //    using (var stream = new StreamWriter(request.GetRequestStream()))
            //    {
            //        var serialized = parameters.ToString();
            //        stream.Write(serialized);
            //    }
            //}

            //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //int code = (int)response.StatusCode;
            //
            //return (new HttpResponse(GetRequestResult(response), code, response.Headers)).WithCookies(response.Cookies);
        }

        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool InternetGetCookieEx(string pchURL, string pchCookieName, StringBuilder pchCookieData, ref uint pcchCookieData, int dwFlags, IntPtr lpReserved);
        const int INTERNET_COOKIE_HTTPONLY = 0x00002000;
        public static string GetGlobalCookies(string uri)
        {
            uint datasize = 1024;
            StringBuilder cookieData = new StringBuilder((int)datasize);
            if (InternetGetCookieEx(uri, null, cookieData, ref datasize, INTERNET_COOKIE_HTTPONLY, IntPtr.Zero)
                && cookieData.Length > 0)
            {
                return cookieData.ToString().Replace(';', ',');
            }
            else
            {
                return null;
            }
        }

        private static string GetRequestResult(HttpWebResponse response)
        {
            
            Stream responseStream = response.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);

            string pageContent = myStreamReader.ReadToEnd();

            myStreamReader.Close();
            responseStream.Close();

            response.Close();

            return pageContent;
        }
        
        private static string GetParamsStr(List<string> parameters) {
            string paramStr = String.Empty;

            for(int i=0; i< parameters.Count; i++) {
                paramStr += (i == 0) ? parameters[i] : ("&" + parameters[i]);
            }

            return paramStr;
        }

        private static string GetCompleteUrl(string url, string method, dynamic parameters)
        {
            if (method.ToLower() == "get")
            {
                return String.Format("{0}?{1}", url, parameters);
            }

            return url;
        }

    }
}
