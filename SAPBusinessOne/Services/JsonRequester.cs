﻿
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using System.Web;
using static SAPBusinessOne.Services.WebRequester;

namespace SAPBusinessOne.Services
{
    public static class JsonRequester
    {
        public static JObject Request(string url, string method, dynamic parameters, WebHeaderInfo headerInfo)
        {
            HttpResponse response = WebRequester.GetResponseAsync(url, headerInfo, method, parameters);

            try
            {
                return JObject.Parse(response.content);
            }
            catch
            {
                return new JObject();
            }
            
        }
    }
}
