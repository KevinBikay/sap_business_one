﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPBusinessOne.Services.Server
{
    public class TransactionStatus
    {
        public string Message
        {
            get;
            private set;
        }

        public bool IsSuccess
        {
            get;
            private set;
        }

        public TransactionStatus(string message, bool isSuccess)
        {
            Message = message;
            IsSuccess = isSuccess;
        }

        public string ToJson()
        {
            var msg = "{\"ok\": " + (IsSuccess ? "true" : "false");

            Message += " (From SAP)";

            Message = Message.Replace('"', '\'');
            Message = Message.Replace("\\r\\n", " | ");
            Message = Message.Replace(Environment.NewLine, " | ");

            if (!IsSuccess) msg += string.Format(",\"error\": \"{0}\"", Message);
            else msg += string.Format(",\"message\": \"{0}\"", Message);

            msg += "}";

            return msg;
        }
    }
}
