﻿using Newtonsoft.Json.Linq;
using SAPBusinessOne.Services.Logs;
using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SAPBusinessOne.Services.Server
{
    public class TcpServer
    {
        public delegate TransactionStatus OnReceiveDataEventHandler(object sender, string data);
        public event OnReceiveDataEventHandler OnReceiveData;

        public delegate void OnServerStartedHandler(bool isStarted);
        public event OnServerStartedHandler OnServerStarted;

        private BackgroundWorker bwListener;
        private IPAddress serverIP;
        private int serverPort;

        private bool _isRunning;
        private TcpListener _server;

        public TcpServer(IPAddress ip, int port)
        {
            serverIP = ip;
            serverPort = port;

            _server = new TcpListener(ip, port);

            bwListener = new BackgroundWorker();
        }

        public bool isPortAvailable(int port)
        {
            // Evaluate current system tcp connections. This is the same information provided
            // by the netstat command line application, just in .Net strongly-typed object
            // form.  We will look through the list, and if our port we would like to use
            // in our TcpClient is occupied, we will set isAvailable to false.
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

            foreach (TcpConnectionInformation tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port == port)
                {
                    return false;
                }
            }

            return true;
        }

        public void Start(OnReceiveDataEventHandler callback, OnServerStartedHandler onServerStarted, RunWorkerCompletedEventHandler onComplete)
        {
            OnReceiveData += callback;
            OnServerStarted += onServerStarted;
            bwListener.WorkerSupportsCancellation = true;
            bwListener.DoWork += new DoWorkEventHandler(StartToListen);
            bwListener.RunWorkerCompleted += onComplete;
            bwListener.RunWorkerAsync();
        }

        public void Stop()
        {
            _server.Stop();
            _isRunning = false;
            bwListener.CancelAsync();
            GC.Collect();
        }

        public bool IsRunning()
        {
            return _isRunning;
        }

        private void StartToListen(object sender, DoWorkEventArgs e)
        {
            try
            {
                _server.Start();
                _isRunning = true;

                OnServerStarted?.Invoke(true);
            }
            catch
            {
                OnServerStarted?.Invoke(false);
                return;
            }

            Thread th = new Thread(ListenClients);
            th.Start();
        }

        private void ListenClients()
        {
            while (_isRunning)
            {
                try
                {
                    // wait for client connection
                    TcpClient newClient = _server.AcceptTcpClient();

                    // client found.
                    // create a thread to handle communication
                    Thread t = new Thread(new ParameterizedThreadStart(HandleClient));

                    t.Start(newClient);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void HandleClient(object obj)
        {
            // retrieve client from parameter passed to thread
            TcpClient client = (TcpClient)obj;
            
            using (client)
            using (NetworkStream stream = client.GetStream())
            using (StreamReader rd = new StreamReader(stream))
            using (StreamWriter wr = new StreamWriter(stream))
            {
                try
                {
                    string sData = rd.ReadLine();

                    if (!string.IsNullOrEmpty(sData))
                    {
                        var result = OnReceiveData.Invoke(this, sData);
                        wr.WriteLine(result.ToJson());
                    }
                }
                catch
                {
                    client.Close();
                }
                finally
                {
                    wr.Flush();
                }
            }
        }
    }
}
